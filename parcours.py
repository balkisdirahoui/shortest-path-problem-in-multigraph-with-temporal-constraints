import numpy as np
import random
import re
from gurobipy import *
from matplotlib import pyplot as plt
import pydot
import graphviz
import networkx as nx
from typing import List
from time import time

from classes import Graph,Vertex,Edge

def build_level(G,source):
  level = []
  level.append([source])
  temp_g = Graph(G.vertices,G.edges)
  vs = [i for i in G.vertices]
  while len(vs):
    temp_l =[]
    new_vs = []
    for v in vs:
        if not(len(temp_g.predecessors(v))):
            temp_l.append(v)
        else:
            new_vs.append(v)
    level.append(temp_l)
    for v in temp_l:
        temp_g = Graph.suppVertex(temp_g,v)
    vs = new_vs
  return level

def djikstra(G,source,dest):
  levels = build_level(G,source)
  levels[0] = [source]
  #dict storing the cost of each vertex
  costs = {}
  #dict storing the predecessor of each vertex
  preds = {}
  for i in G.vertices:
    costs[i.name] = -1
  costs[source.name] = 0
  for i in range(len(levels)):
    for v in levels[i]:
      for j in G.successors(v):
        #finding the edge with the minimal duration between two vertices
        min_duration = min([e.start for e in G.get_edges(v,j)])
        #checking if the vertex has already been visited or not
        if(costs[j.name]==-1 or costs[v.name]+min_duration < costs[j.name]):
            costs[j.name] = costs[v.name]+min_duration
            preds[j.name] = v
  shortest_path = []
  current = dest
  while current != source and costs[current.name]!=-1:
    shortest_path.append(current.name)
    current = preds[current.name]
  shortest_path.append(source.name)
  return costs,preds, [i for i in reversed(shortest_path)]

def earliest_arrival_path(G,source,dest):
  sources, dests = G.multi_path(len(G.vertices),source,dest)
  TransG = G.TransGraph(len(G.vertices))
  source_name = "%s_%d"%(min(sources))
  cost = float('inf')
  path= []
  while(len(dests)):
    dest_name = "%s_%d"%(min(dests))
    del dests[dests.index(min(dests))]
    dj_results = djikstra(TransG,TransG.vertices[TransG.vertices.index(Vertex(source_name))],TransG.vertices[TransG.vertices.index(Vertex(dest_name))])
    tmp = dj_results[0][dest_name]
    if(tmp >0 and tmp <cost):
      cost = tmp
      path = dj_results[2]
  return cost,path


def latest_takeoff_path(G,source,dest):
  sources, dests = G.multi_path(len(G.vertices),source,dest)
  TransG = G.TransGraph(len(G.vertices))
  dest_name = "%s_%d"%(max(dests))
  cost = float('inf')
  path= []
  while(len(sources)):
    source_name = "%s_%d"%(max(sources))
    del sources[sources.index(max(sources))]
    dj_results = djikstra(TransG,TransG.vertices[TransG.vertices.index(Vertex(source_name))],TransG.vertices[TransG.vertices.index(Vertex(dest_name))])
    tmp = dj_results[0][dest_name]
    if(tmp >0 and tmp <cost):
      cost = tmp
      path = dj_results[2]
  return cost,path


def quickest_path(G,source,dest):
  sources, dests = G.multi_path(len(G.vertices),source,dest)
  TransG = G.TransGraph(len(G.vertices))
  cost = float('inf')
  path= []
  while(len(sources)):
    temp = [i for i in dests]
    source_name = "%s_%d"%(max(sources))
    del sources[sources.index(max(sources))]
    while(len(temp)):
      dest_name = "%s_%d"%(min(temp))
      del temp[temp.index(min(temp))]
      dj_results = djikstra(TransG,TransG.vertices[TransG.vertices.index(Vertex(source_name))],TransG.vertices[TransG.vertices.index(Vertex(dest_name))])
      tmp = dj_results[0][dest_name]
      if(tmp >0 and tmp <cost):
        cost = tmp
        path = dj_results[2]
  return cost,path




def shortest_path(G,source,dest):
  sources, dests = G.multi_path(len(G.vertices),source,dest)
  t = time()
  TransG = G.TransGraph(len(G.vertices))
  t_transform = time() -t
  source_name = "%s_%d"%(min(sources))
  dest_name = "%s_%d"%(max(dests))
  dj_result = djikstra(TransG,TransG.vertices[TransG.vertices.index(Vertex(source_name))],TransG.vertices[TransG.vertices.index(Vertex(dest_name))])
  TransG.display_path(dj_result[2])
  return dj_result[0][dest_name],dj_result[2], t_transform


def shortest_path_pl(G,source,dest):
  n = len(G.vertices)
  sources, dests = G.multi_path(n,source,dest)
  t = time()
  G = G.TransGraph(len(G.vertices))
  t_transform = time() -t
  source_name = "%s_%d"%(min(sources))
  dest_name = "%s_%d"%(max(dests))
  source, dest = Vertex(source_name), Vertex(dest_name)
  lp = Model("mogplex")
  lp.Params.OutputFlag = 0
  #vars of the linear program
  vars = []
  #coefficients of the obj function
  coeffs = []
  for e in G.edges:
    vars.append(lp.addVar(vtype=GRB.BINARY,name="x%s_%s" %(e.source[0].name,e.dest[0].name)))
    coeffs.append(e.start)
  lp.update()
  #creating the obj function
  obj = LinExpr();
  obj =0
  for i in range(len(vars)):
    obj += coeffs[i] * vars[i]
  lp.setObjective(obj,GRB.MINIMIZE)
  #constraint on intermediate edges (making sure that each time we get in an edge we get out of it)
  intermediate_vertices = [ i for i in G.vertices if i!= source and i!= dest]
  constraints = []
  cpt = 0
  for v in intermediate_vertices:
    preds = G.predecessors(v)
    succs = G.successors(v)
    expr = LinExpr()
    for pred in preds:
      expr.add(lp.getVarByName("x%s_%s" %(pred.name,v.name)),-1)
    for succ in succs:
      expr.add(lp.getVarByName("x%s_%s" %(v.name,succ.name)),1)
    constraints.append(lp.addConstr(expr, GRB.EQUAL,0,"c%d"%(cpt)))
    cpt+=1
  #constraint for the destination vertex (making sure that we only access it once)
  expr = LinExpr()
  preds = G.predecessors(dest)
  for pred in preds:
      expr.add(lp.getVarByName("x%s_%s" %(pred.name,dest.name)),1)
  constraints.append(lp.addConstr(expr, GRB.EQUAL,1,"c%d"%(cpt)))
  lp.update()
  path=[]
  lp.optimize()
  if(lp.getAttr("SolCount")>0):
    for v in lp.getVars():
      if(v.getAttr("X")==1):
        path.append(re.match(r"x(.+_[0-9]+)_(.+_[0-9]+)",v.getAttr('VarName')).group(1))
        path.append(re.match(r"x(.+_[0-9]+)_(.+_[0-9]+)",v.getAttr('VarName')).group(2))
    p = []
    for  i in path:
      if i  not in p:
        p.append(i)
    return p, t_transform
  return -1,t_transform
